"""
YOU CAN READ THE README FOR MORE INFO ABOUT THIS SCRIPT.
Overview:
This script is performing optimization on the parameters of a Permanent Magnet Synchronous Motor (PMSM) simulation 
using the Nelder-Mead and basinhopping methods from the scipy.optimize library. It first sets some initial values 
for the simulation parameters, runs the simulation and saves the resulting current values to be used as a reference 
for optimization. 
Then, runs the simulation and calculates the error between the current values of this new simulation and
the reference current values. This error is then minimized using the Nelder-Mead and basinhopping methods, with bounds
and initial guesses specified. 
The final result of the optimization is then printed.

## Understanding the Simulation Parameters
The ```run_simulation``` function takes 9 parameters which corresponds to the internal characteristic of the PMSM Motor. 
the following parameters are used in the simulation :
- Kt # Torque constant [Nm/A]
- R  # Phase Resistance [Ohm]
- L  # Phase Inductance [H]
- J  # Rotor Inertia [kg*m^2]
- B  # Friction [Nm/(rad/s)]
- Imax  # Courant Max [A]
- pole  # Nombre de poles (integer)
- loadt #Torque de charge [Nm]
- loadj #Inertie de charge [kg*m^2]

### Changing the Reference Simulation
To change the values of the real simulation, simply change the values of that results in the ```real_sim_df``` DataFrame. The ```real_sim_df``` DataFrame is used as a reference for the optimization.
### Changing the initial guess and bounds for the optimization
To change the initial guess and bounds for the optimization, simply change the values of the ```initial_guess``` and ```bounds``` variables in the ```Optimize_tool.py``` main script. The ```initial_guess``` variable is the initial guess for the optimization, and the ```bounds``` variable is the bounds for the optimization.


#How to use the run_simulation function:
The run_simulation function is used to perform the PMSM simulation and return the resulting current values as a Pandas DataFrame. 
This function is located in the simulation module and is imported at the top of this script.
To use this function, you need to pass the simulation parameters such as pole, Kt, R, L, B, J, loadt, loadj
to the function as arguments. The returned result will be a pandas DataFrame containing the current values of the 
simulation.

#How to update the run_simulation function:
To update the run_simulation function, you need to modify the code located in the top to update the run_simulation function to your own

#How to change the number of params to look for:
The number of parameters to optimize is defined in the func_optimize_pmsm function. 
The function takes a list of parameters as arguments, which are then unpacked and used to run the simulation. 
To change the number of parameters to optimize, you need to modify the argument list and the unpacking code in 
the function accordingly. The bounds and initial guesses of the parameters are defined in the main block and 
passed to the optimization methods. You will also need to update the bounds and initial guesses to match the 
number of parameters being optimized.
"""

from simulation import run_simulation  # Plug-in the run_simulation function to your own API
import numpy as np
import pandas as pd
import numpy.random as rd
import scipy.optimize as spo
from scipy.optimize import basinhopping

# real_sim_df DataFrame is used as a reference for the optimization
"""
To change the values of the real simulation, simply change the following values 
that results in the real_sim_df DataFrame. 
"""

pole_pairs, Kt, R, L, B, J, loadt, loadj = (1, 0.17, 2.13, 3.4, 0.034, 0.45, 0.79, 0.005)
real_sim_df = run_simulation(pole_pairs, Kt, R, L, B, J, loadt, loadj)  # Dataframe of the real simulation
vraiIA = real_sim_df.ia
vraiIB = real_sim_df.ib
vraiIC = real_sim_df.ic


def run_simulation_args(Kt, R, L, B, J):
    """
    To update the PMSM simulation, simply override the ```run_simulation_args``` to your own API.
    Your API needs to return a Pandas DataFrame with at least the following columns : time, ia, ib, ic
    those are the important columns of the simulation, you can add more columns if you want. 
    """
    # Set the fixed simulation parameters
    pole_pairs, loadt, loadj = (1, 0.79, 0.005)
    # Run the simulation and return a pandas DataFrame that contains at least the current values ia, ib, ic
    return run_simulation(pole_pairs, Kt, R, L, B, J, loadt, loadj)


# Reset the simulation parameters to None
pole_pairs, Kt, R, L, B, J, loadt, loadj = (None, None, None, None, None, None, None, None)


def func_optimize_pmsm(args):
    """
    This function is used to optimize the simulation parameters.
    it takes a list of parameters as arguments 5 Floats: (Kt, R, L, B, J)
    runs the simulation and returns the Distance between the current values
    of the simulation and the reference current values.
    """
    # Set the parameters to optimize
    Kt, R, L, B, J = args  # the 5 parameters to optimize
    # Run the simulation and return the result which is a pandas DataFrame that contains the current values ia, ib, ic
    run = run_simulation_args(Kt, R, L, B, J)
    ia = run.ia
    ib = run.ib
    ic = run.ic
    # Calculate the error between the current values of the simulation and the reference current values
    distance = np.sum((ia-vraiIA)**2) + np.sum((ib-vraiIB)**2) + np.sum((ic-vraiIC)**2)
    # optional: print the distance error
    print("Error:", distance)
    return distance


if __name__ == "__main__":
    """
    To change the initial guess and bounds for the optimization, 
    simply change the values of the initial_guess and bounds variables in the Optimize_tool.py main script. 
    The initial_guess variable is the initial guess for the optimization
     and the bounds variable is the bounds for the optimization.
    """
    # optimize with Nelder-Mead
    # the initial guess : parameters that are realistic.
    initial_guess = np.array([0.01, 0.025, 1.3, 0.025, 1.3])
    # the bounds : that are realistic.
    bounds = [(1.0e-5, 30.0)]
    bounds = bounds * len(initial_guess)  # times the number of parameters
    res = spo.minimize(func_optimize_pmsm, initial_guess, method='Nelder-Mead', bounds=bounds, options={'disp': True})
    print("Minimize 0 : with Nelder-Mead...")
    print(res.x)
    initial_guess = res.x
    Temp = int(res.fun)
    # optimize with basinhopping 10 iterations
    res = basinhopping(func_optimize_pmsm,
                       initial_guess,
                       minimizer_kwargs={"method": "Nelder-Mead", "bounds": bounds},
                       niter=10,
                       disp=True,
                       T=Temp)
    print(res)
    print(res.x)
