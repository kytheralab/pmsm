"""
OVERVIEW:
This code provides an example of how to use the PMSM simulation function run_simulation from the simulation module.
The simulation calculates the current (ia, ib, ic) in a permanent magnet synchronous machine (PMSM) with the given input parameters.
The code also compares two simulation results and plots one of the currents for visualization.
"""

from simulation import run_simulation
import numpy as np
import matplotlib.pyplot as plt


if __name__ == "__main__":
    pole_pairs, Kt, R, L, B, J, loadt, loadj = (1, 0.16713142, 2.14597104, 3.42167213, 0.03396446, 0.44996109, 0.79, 0.005)
    my_df1 = run_simulation(pole_pairs, Kt, R, L, B, J, loadt, loadj)
    pole_pairs, Kt, R, L, B, J, loadt, loadj = (1, 0.17, 2.13, 3.4, 0.034, 0.45, 0.79, 0.005)
    my_df2 = run_simulation(pole_pairs, Kt, R, L, B, J, loadt, loadj)
    ia1 = my_df1.ia
    ib1 = my_df1.ib
    ic1 = my_df1.ic
    ia2 = my_df2.ia
    ib2 = my_df2.ib
    ic2 = my_df2.ic
    x = np.sum((ia1-ia2)**2) + np.sum((ib1-ib2)**2) + np.sum((ic1-ic2)**2)
    print("Error is:", x)
    plt.plot(my_df1[['ib']], label='Optimized')
    plt.plot(my_df2[['ib']], label='Real')
    plt.legend()
    plt.show()
