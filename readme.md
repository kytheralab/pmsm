# Permanent Magnet Synchronous Motor (PMSM) Optimization
This script performs optimization on the parameters of a Permanent Magnet Synchronous Motor (PMSM) simulation using the Nelder-Mead and basinhopping methods from the scipy.optimize library. It first sets some initial values for the simulation parameters, runs the simulation and saves the resulting current values to be used as a reference for optimization. The script then calculates the error between the current values of a new simulation run and the reference current values, which is then minimized using the Nelder-Mead and basinhopping methods.
Note: ```simulation``` & ```motor_model``` scripts contain the internal work of the ```run_simulation``` function that provides a simulation of a PMSM motor.

## Prerequisites
- numpy
- pandas
- numpy.random
- scipy.optimize

## Understanding the Simulation Parameters
The ```run_simulation``` function takes 9 parameters which corresponds to the internal characteristic of the PMSM Motor. 
the following parameters are used in the simulation :
- Kt # Torque constant [Nm/A]
- R  # Phase Resistance [Ohm]
- L  # Phase Inductance [H]
- J  # Rotor Inertia [kg*m^2]
- B  # Friction [Nm/(rad/s)]
- Imax  # Courant Max [A]
- pole  # Nombre de poles (integer)
- loadt # Torque de charge [Nm]
- loadj # Inertie de charge [kg*m^2]

The function returns a Pandas DataFrame with at least the following columns :
```time, ia, ib, ic``` (in seconds, in Amps) with the ```time``` itself as the index.

## Usage Instructions for the Optimize_tool.py Script
### Running the Script
To run the script, use the following command :
```python
python optimizer_tool.py
```
The script will try to figure out the best parameters for the simulation to match the reference simulation.
in the best case, the script will find the same parameters as the reference simulation.

### Changing the Reference Simulation
To change the values of the real simulation, simply change the values that results in the ```real_sim_df``` DataFrame. 
The ```real_sim_df``` DataFrame is used as a reference for the optimization.

Out of the box, the script will try to figure out the best parameters for the simulation to match the reference simulation.
in the best case, the script will find the same parameters as the reference simulation.
The script will then print these parameters.

### Changing the initial guess and bounds for the optimization
To change the initial guess and bounds for the optimization, simply change the values of the ```initial_guess``` and ```bnds``` variables in the ```Optimize_tool.py``` main script. The ```initial_guess``` variable is the initial guess for the optimization, and the ```bnds``` variable is the bounds for the optimization.

- Note 1: The number of parameters in the ```initial_guess``` and ```bnds``` variables must match the number of parameters in the ```run_simulation_args``` function.

- Note 2: The ```initial_guess``` and ```bnds``` for the optimization must be realistic values for the simulation.

### How the Script Works in Detail
The ```Optimize_tool.py``` script first sets the fixed parameters for the PMSM simulation using the ```run_simulation``` function from the ```simulation``` module. The ```run_simulation``` function takes 9 parameters, and the script sets some initial values for these parameters.

The ```run_simulation_args``` function takes 5 parameters to be optimized, which are then passed to the ```run_simulation``` function to run the simulation.

The ```func_optimize_pmsm``` function takes the optimized parameters, runs the simulation, and calculates the error between the current values of the new simulation run and the reference current values. The error is then returned by the function.

The script then performs optimization using the ```Nelder-Mead``` method and the ```basinhopping``` method. The initial guess for the optimization is set to a realistic set of parameters, and the bounds for these parameters are also set. The final result of the optimization is then printed.

## Updating/overriding the Simulation
### Overriding the Simulation API
To update the PMSM simulation, simply override the ```run_simulation_args``` to your own API.
Your API needs to return a Pandas DataFrame with at least the following columns :
```time, ia, ib, ic``` (in seconds, in Amps) and the following index : ```time```. The ```run_simulation``` function will then use this DataFrame to calculate the error between the current values of the new simulation run and the reference current values. The error is then returned by the function. 

### Changing the Number of Parameters to Optimize
To change the number of parameters to optimize, simply update the number of parameters in the ```run_simulation_args``` and ```func_optimize_pmsm``` functions to match the desired number of parameters. The initial guess and bounds for the optimization must also be updated accordingly.


## Usage Instructions for the plotter.py Script
### Running the Script
To run the script, use the following command :
```python
python plotter.py
```
The script will plot the current values of the reference simulation and the optimized simulation.
You need to change the ```pole, Kt, R, L, B, J, loadt, loadj``` values to match the values of the simulation you want to plot.

Generally, you plot using the values of the optimized simulation, and you compare it to the reference simulation.