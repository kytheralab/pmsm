import math

"""
Motor model parameters
     Kt # Torque constant [Nm/A]
     R  # Phase Resistance [Ohm]
     L  # Phase Inductance [H]
     J  # Rotor Inertia [kg*m^2]
     B  # Friction [Nm/(rad/s)]     
     Imax # Courant Max [A]
     pole_pairs # Nombre de paires de poles (integer >= 1)
"""


class PID:

    def __init__(self, Kp, Ki, Kd):
        self.Kp = Kp
        self.Ki = Ki
        self.Kd = Kd
        self.P = 0
        self.I = 0
        self.D = 0
        self.error_1 = 0
        self.output = 0

    def update(self, error):
        self.P = error * self.Kp
        self.I += error * self.Ki
        self.D = (error - self.error_1) * self.Kd
        self.error_1 = error
        if self.I > 1:
            self.I = 1
        elif self.I < -1:
            self.I = -1
        self.output = self.P + self.I + self.D
        if self.output > 1:
            self.output = 1
        elif self.output < -1:
            self.output = -1.0
        return self.output


class Motor:

    def __init__(self):
        self.ia = 0.0  # phase currents
        self.ib = 0.0
        self.ic = 0.0
        self.ialpha = 0.0  # ialpha, ibeta, id, iq currents
        self.ibeta = 0.0
        self.id = 0.0
        self.iq = 0.0
        self.van = 0.0  # phase voltages
        self.vbn = 0.0
        self.vcn = 0.0
        self.bemfa = 0.0  # back-emf voltages
        self.bemfb = 0.0
        self.bemfc = 0.0
        self.torque = 0.0  # torque
        self.velocity = 0.0  # motor velocity (radians/sec)
        self.position = 0.0  # motor position (radians)

    def update(self, t, dt, va, vb, vc, load_T, load_J, R, L, pole_pairs, Kt, B, J):

        # Wye connection - phase voltages
        vn = (va + vb + vc) / 3.0
        self.van = va - vn
        self.vbn = vb - vn
        self.vcn = vc - vn

        # resistance of a single phase
        # vL of each phase
        vLa = self.van - self.bemfa - self.ia * R
        vLb = self.vbn - self.bemfb - self.ib * R
        vLc = self.vcn - self.bemfc - self.ic * R
        # inductance of a single phase
        self.ia += vLa / L * dt
        self.ib += vLb / L * dt
        self.ic += vLc / L * dt

        # Wye connection - sum of all currents must be 0 
        iavg = (self.ia + self.ib + self.ic) / 3
        self.ia -= iavg
        self.ib -= iavg
        self.ic -= iavg

        # motor angle
        angle = self.position * pole_pairs
        sina = math.sin(angle)
        sinb = math.sin(angle - math.pi * 2/3)
        sinc = math.sin(angle - math.pi * 4/3)

        # torque
        torque = (self.ia * sina + self.ib * sinb + self.ic * sinc) * Kt

        # Clarke Transform
        sqrt3inv = 3**(-1/2)  # 1/sqrt(3)
        self.ialpha = self.ia
        self.ibeta  = sqrt3inv * (self.ia + 2 * self.ib)

        # Park Transform
        s1 = math.sin(angle)
        c1 = math.cos(angle)
        self.id = c1 * self.ialpha + s1 * self.ibeta
        self.iq = -s1 * self.ialpha + c1 * self.ibeta

        # update motor velocity and position 
        self.velocity += (torque - self.velocity * B - load_T) / (J + load_J) * dt
        #print("velocity: "+ str(self.velocity))
        self.position += self.velocity * dt

        # back-emf voltages
        Kv = 1 / Kt # assumes Kv = 1 / Kt (ideal motor)
        self.bemfa = sina * self.velocity / Kv
        self.bemfb = sinb * self.velocity / Kv
        self.bemfc = sinc * self.velocity / Kv
        # torque
        self.torque = torque
