#imports from global libraries installed using pip.
import pandas as pd
import math
from datetime import timedelta

from motor_model import PID, Motor


def calc_openloop_voltages(t, vbus, rpm, n_pole_pairs):
    """
    calculate va, vb, vc for an openloop (constant speed) control scheme
    """
    omega = 2 * math.pi * rpm / 60 * n_pole_pairs  # 2*math.pi*f/P
    va = vbus * math.sin(t * omega)
    vb = vbus * math.sin(t * omega - math.pi * 2/3)
    vc = vbus * math.sin(t * omega - math.pi * 4/3)
    return va, vb, vc

# External config
controller = 'open'
vbus = 19.81
rpm = 50

# Simulation config
simulation_time = 10  # in seconds
dt = 0.00005  # sampling time


def draw(simulation_time, motor, position, controller, vbus, rpm, rpm_pid,
         loadt, loadj, pole_pairs, Kt, R, L, B, J):
    """
    @param simulation_time: simulation_time in seconds
    @param motor: a Motor instance.
    @param position: initial position state at t=0
    @param controller: default to open for this simulation
    @param vbus: config = max voltage in each phase of the motor
    @param rpm: config = rotations per minute
    @param rpm_pid: PID is the corrector that will take the expected
                    value and converge towards it reducing the error
    @param loadt
    @param loadj: defined in config

    @returns a dataframe with the following columns:
    t, va, vb, vc, ia, ib, ic, position, rpm
    """
    t = 0
    d = []
    delta = timedelta(seconds=simulation_time)
    delta = delta.total_seconds()
    step = delta / dt
    for i in range(int(step)):
        angle = position * pole_pairs
        if controller == 'open':
            va, vb, vc = calc_openloop_voltages(t, vbus, rpm, pole_pairs)

        motor.update(t, dt, va, vb, vc, loadt, loadj, R, L, pole_pairs, Kt, B, J)
        t += dt
        position = motor.position
        ia = motor.ia
        ib = motor.ib
        ic = motor.ic
        d.append((t, va, vb, vc, ia, ib, ic, position, rpm))
    return pd.DataFrame(d, columns=('t', 'va', 'vb', 'vc', 'ia', 'ib', 'ic', 'position', 'rpm'))


def run_simulation(pole_pairs, Kt, R, L, B, J, loadt, loadj):
    # initial states : reset to 0 before each simulation
    position = 0.0
    motor = Motor()
    rpm_pid = PID(5, 1e-5, 1000)
    return draw(simulation_time, motor, position, controller, vbus, rpm,
                rpm_pid, loadt, loadj, pole_pairs, Kt, R, L, B, J)
